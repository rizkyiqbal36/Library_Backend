export enum UpdateBookEnum {
  ADD = 'ADD',
  DELETE = 'DELETE'
}

export enum BookStatusEnum {
  WAIT = 'WAIT',
  CANCEL = 'CANCEL',
  ACTIVE = 'ACTIVE'
}
